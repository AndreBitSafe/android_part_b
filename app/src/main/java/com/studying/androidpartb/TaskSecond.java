package com.studying.androidpartb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class TaskSecond extends AppCompatActivity {

    private EditText login;
    private EditText email;
    private EditText phone;
    private EditText passFirst;
    private EditText passSecond;

    private TextView loginErr;
    private TextView emailErr;
    private TextView phoneErr;
    private TextView passErr;

    private Button checkBut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_second);

        login = findViewById(R.id.login);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        passFirst = findViewById(R.id.pass_first);
        passSecond = findViewById(R.id.pass_second);

        loginErr = findViewById(R.id.login_error);
        emailErr = findViewById(R.id.email_error);
        phoneErr = findViewById(R.id.phone_error);
        passErr = findViewById(R.id.pass_error);

        checkBut = findViewById(R.id.but_check);

        checkBut.setOnClickListener(v -> {
            if (checkFields()) {
                Toast.makeText(getApplicationContext(), "No errors", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean checkFields() {
        boolean error = true;

        if (login.getText().toString().isEmpty()) {
            loginErr.setVisibility(View.VISIBLE);
            error = false;
        } else {
            loginErr.setVisibility(View.GONE);
        }

        if (!EmailValidator.validate(email)) {
            emailErr.setVisibility(View.VISIBLE);
            error = false;
        } else {
            emailErr.setVisibility(View.GONE);
        }

        if (!PhoneValidator.validate(phone)) {
            phoneErr.setVisibility(View.VISIBLE);
            error = false;
        } else {
            phoneErr.setVisibility(View.GONE);
        }

        if (passFirst.getText().toString().isEmpty()) {
            passErr.setText("*Write password");
            passErr.setVisibility(View.VISIBLE);
            error = false;
        } else if (!passFirst.getText().toString()
                .equals(passSecond.getText().toString())) {
            passErr.setText("*Passwords do not match");
            passErr.setVisibility(View.VISIBLE);
            error = false;
        } else {
            passErr.setVisibility(View.GONE);
        }

        return error;
    }
}
