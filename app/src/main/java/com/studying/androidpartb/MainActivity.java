package com.studying.androidpartb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.PriorityQueue;

public class MainActivity extends AppCompatActivity {

    private final static int STACK_SIZE = 5;

    private EditText editText;

    private Button retWord;
    private Button saveWord;

    private LinkedList<String> stack = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.text_word);

        retWord = findViewById(R.id.return_but);
        saveWord = findViewById(R.id.save_but);

        retWord.setOnClickListener(v -> {
            if (!stack.isEmpty()) {
                editText.setText(stack.pollLast());
            } else {
                // do something
            }
        });

        saveWord.setOnClickListener(v -> {
            boolean textFieldIsEmpty = editText.getText().toString().isEmpty();
            if (!textFieldIsEmpty) {
                if (stack.size() < STACK_SIZE) {
                    stack.add(editText.getText().toString());
                    editText.setText("");
                } else {
                    stack.add(editText.getText().toString());
                    stack.removeFirst();
                    editText.setText("");
                }
            }
        });
    }

    public void nextTask(View v) {
        startActivity(new Intent(MainActivity.this, TaskSecond.class));
    }
}
