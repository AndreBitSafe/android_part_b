package com.studying.androidpartb;

import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class EmailValidator {

    private static final String EMAIL__PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
                    "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    ;

    private EmailValidator() {
    }

    public static boolean validate(final EditText init) {
        String initStr = init.getText().toString();
        if (initStr.isEmpty()) {
            return false;
        }
        Pattern pattern = Pattern.compile(EMAIL__PATTERN);
        Matcher matcher = pattern.matcher(initStr);
        return matcher.matches();

    }
}
