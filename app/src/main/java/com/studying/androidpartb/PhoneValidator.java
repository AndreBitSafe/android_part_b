package com.studying.androidpartb;

import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class PhoneValidator {
    private PhoneValidator() {
    }

    public static boolean validate(final EditText init) {
        String initSrt = init.getText().toString();
        if (initSrt.startsWith("+380") && init.length() == 13) {
            String numbers = initSrt.replace("+380", "");
            Pattern p = Pattern.compile("[0-9]");
            Matcher m = p.matcher(numbers);
            if (!m.matches()) {
                return false;
            }
            return true;
        }
        return false;
    }
}
